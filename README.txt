--Make sure Java is installed and JAVA_HOME is set.
--Good if internet is working.
--Make sure PostgresSQL is installed on the system.

--Before running the project follow these steps--
	1.Extract vizMovie.tar file.
	2. Go inside vizMovie folder and open build.gradle file in a editor.
	3. look for following line:-  
		dirs '/home/neelesh/Projects/vizMovie/src/main/webapp/WEB-INF/lib'
		and change the path according to your vizMovie directory.
		exp:- /<addresss to vizMovie folder>/vizMovie/src/main/webapp/WEB-INF/lib'


	--setting up postgres database.
	1. open pgAdmin
	2. Create a database name movieDatabase
	3. Create a schema in this database named `public` (this is usually provided by default).
	4. Create a table in the above schema called `movie_table`
	5. Create 7 columns in the table:id(make it primarykey), title, yearr, director, castt, about, image_path
	
	6. Go inside /vizMovie/src/main/resources folder.
	7. open application.properties
	8  set database url to the url on which postgresql is running.;
	9. set your postgressql username and passworrd.
	
--Running the application.
	1. go to vizMovie folder.
	2. open terminal here.
	3. run following command.
		./gradlew bootrun
	 application will start building.
	once finished open follwing url in your browser
	
	http://localhost:8080

--Description about project modules.
	-there are many unecessary file in project so explaining that are used.
	
	
	--vizMovie/src/main/java/com/vizexperts/springtutorialapp/Movie.java => To hold the structure of movie details.
	--vizMovie/src/main/java/com/vizexperts/springtutorialapp/RootController.java => Contains the services like getting list of movies available in DB and inserting new movie to database.
	--vizMovie/src/main/java/com/vizexperts/springtutorialapp/db/UserDAO.java => Actual code of getting the list of movies from DB and Inserting new movie to DB.
	--vizMovie/src/main/resources/assetes/validater.js => contain all(everything) the client side logic(javascript) code. Basically this assets folder contains whole client code and libraries.
	--vizMovie/src/main/webapp/pages/movie.jsp => html or jsp code to render everythingh on the screen.
