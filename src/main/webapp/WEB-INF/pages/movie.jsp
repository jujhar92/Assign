    <!DOCTYPE HTML>
        <!--
        Strata by HTML5 UP
        html5up.net | @n33co
        Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
        -->
        <html>
        <head>
        <title>VizExperts MovieUI</title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <!--[if lte IE 8]><script src="/assets/js/ie/html5shiv.js"></script><![endif]-->
        <link rel="stylesheet" href="/assets/css/main.css" />
        <!--[if lte IE 8]><link rel="stylesheet" href="/assets/css/ie8.css" /><![endif]-->
        </head>
        <body id="top">
        <a href="#" name="top"></a>
        <!-- Header -->
        <header id="header">
        </header>

        <!-- Main -->
        <div id="main">

        <!-- One -->
        <section id="one">
        <header class="major">

        <hr>
        <h2 id="titleMovie">Title : --</h2>
        <h3 id="directorMovie">Director: --</h3>
        <h3 id="yearMovie">Production Year : --</h3>
        <h3 id="castMovie">Cast: -- </h3>
        </header>
        <p id="aboutMovie">--</p>
        <ul class="actions">
        <li><a href="#aMovie" class="button">Add Movie</a></li>
        <li><a href="#bMovie" class="button">Browse Movie</a></li></ul>
        </section>

        <!-- Two -->
        <section id="two">
        <a href="#" name="bMovie"></a>
        <h2>Browse Movies</h2>
        <hr>
        <div id="movieList" class="row">
        </div>
        </section>

        <!-- Three -->
        <section id="three">
        <a href="#" name="aMovie" style=""></a>
        <h2>Add Movie</h2>
        <p>&nbsp;</p>
        <div class="row">
        <div class="8u 12u$(small)">
        <form method="post" action="#">
        <div class="row uniform 50%">
        <div class="6u 12u$(xsmall)"><input type="text" name="Name" id="Name" placeholder="Title" /></div>
        <div class="6u$ 12u$(xsmall)"><input name="Director" type="text" id="Director" value="" placeholder="Director" /></div>
        <div class="12u$ 12u$(xsmall)"><input name="year" type="text" id="year" value="" placeholder="Year" /></div>
        <div class="12u$ 12u$(xsmall)"><input name="cast" type="text" id="cast" value="" placeholder="Cast" /></div>
        <div class="12u$">
        <textarea name="Other Information" id="about" placeholder="About"></textarea>
        </div>
        <div class="12u$ 12u$(xsmall)" style="margin-top:5px;padding-top:0px;"> <label id="errorLabel" style="color:red; display:none"> error message</div>
        </div>
        </form>
        <ul class="actions">
        <li><input type="submit" value="Submit" onclick="onSubmitClick()" /></li>
        </ul>
        </div>

        </div>
        </section>
        </div>

        <!-- Footer -->
        <footer id="footer">
        <ul class="icons">
        <li><a href="#" class="icon fa-twitter"><span class="label">Twitter</span></a></li>
        <li><a href="#" class="icon fa-github"><span class="label">Github</span></a></li>
        <li><a href="#" class="icon fa-dribbble"><span class="label">Dribbble</span></a></li>
        <li><a href="#" class="icon fa-envelope-o"><span class="label">Email</span></a></li>
        </ul>
        <ul class="copyright">
        <li>&copy; VizExperts</li>
        <li>2015<a href="http://html5up.net"></a></li>
        </ul>
        </footer>

        <!-- Scripts -->
        <script src="/assets/js/jquery.min.js"></script>
        <script src="/assets/js/jquery.poptrox.min.js"></script>
        <script src="/assets/js/skel.min.js"></script>
        <script src="/assets/js/util.js"></script>
        <!--[if lte IE 8]><script src="/assets/js/ie/respond.min.js"></script><![endif]-->
        <script src="/assets/js/main.js"></script>
        <script src="/assets/validater.js"></script>


        </body>
        </html>