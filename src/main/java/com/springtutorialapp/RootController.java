package com.springtutorialapp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;



@Controller
@RestController
public class RootController {
    @RequestMapping("/")
    public ModelAndView homapage(){
        ModelAndView page  = new ModelAndView();
        page.setViewName("movie");
        return page;
    }

    @RequestMapping("/greeting")
    public ModelAndView greeting(@RequestParam(value="name", required=false, defaultValue="User") String name) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("greeting");
        String str = "Hello " + name + "!";
        mav.addObject("message", str);
        return mav;
    }
}