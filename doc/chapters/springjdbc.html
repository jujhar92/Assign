<!DOCTYPE html>
<html>
<title>Spring WebApp Tutorial</title>

<xmp theme="spacelab" style="display:none;">

# JDBC Using Spring
Accessing database functionality is indispensable for any web application. This tutorial presents a quick introduction to accessing relational databases using Spring. For this sample, we will be using a PostGreSQL database. We will write a simple Web app that takes this data and displays the authorised users as a list using JSP, similar to what we did in the last example on Spring MVC.

Creating a PostGreSQL database, and then creating a table and populating its entries is not in the scope of this tutorial. We assume the following has been completed as regards the database setup needed before we start this sample:

* Create a PostGres database named `authDB`
* Create a schema in this database named `public` (this is usually provided by default)
* Create a table in the above schema called `jdbcauth`
* Create 3 columns in the table: name, password, role
* Populate it with some data

Here is a screenshot of the pgAdmin GUI showing my database setup:
![Database setup pgAdminIII](database_setup_jdbc.png)
Format: ![Alt Screenshot from pgAdmin after database setup](url)

## Gradle configuration

We modify the dependencies list to add three more dependencies:

- `org.springframework.boot:spring-boot-starter-jdbc` dependency which helps automate dependency injection for doing JDBC with Spring
- `postgresql:postgresql` - a JDBC driver for the PostGreSQL database
- `net.sourceforge.jtds:jtds` - JDBC 3.0 driver for Microsoft SQL Server. The PostGreSQL driver seems to depend on this.
- `javax.servlet:jstl` - Java Standard Template library - for parsing JSP templates/pages

`build.gradle`:
```text
    task wrapper(type: Wrapper) {
        gradleVersion = '2.4'
    }

    buildscript {
        repositories {
            mavenCentral()
            maven { url "http://repo.spring.io/release"}
        }

        dependencies {
            classpath("org.springframework.boot:spring-boot-gradle-plugin:1.2.3.RELEASE")
        }
    }

    apply plugin: 'java'
    apply plugin: 'spring-boot'

    repositories {
        mavenCentral()
        maven { url "http://repo.spring.io/release" }
    }

    dependencies {
        compile("org.springframework.boot:spring-boot-starter-web")
        compile("org.springframework.boot:spring-boot-starter-jdbc")
        testCompile("org.springframework.boot:spring-boot-starter-test")
        runtime("org.apache.tomcat.embed:tomcat-embed-jasper")
        runtime("postgresql:postgresql:9.1-901.jdbc4")
        runtime("net.sourceforge.jtds:jtds:1.3.1")
        runtime("javax.servlet:jstl:1.2")
    }
```

## Application Configuration
In addition to the Gradle modification as above, we make a modification to the application's properties in order to tell Spring the details about the database that we will be using.
As in the last sample, we make these modifications in `src/main/resources/application.properties:`
```text
spring.view.prefix: /WEB-INF/pages/
spring.view.suffix: .jsp

spring.datasource.driverClassName=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://localhost:5432/authdb
spring.datasource.username=postgres
spring.datasource.password=123edfr
```
Note that in the above snippet, we specify a `driverClassName`. This represents driver that will be used to connect to the required database. In our case, the dependency that we specified in gradle, specifically, `postgresql:postgresql` provides the implementation of the `org.postgresql.Driver` class that Spring can now instantiate and use to connect to the database with. As you can guess, connecting to any other SQL based relational database (eg. MySQL, Oracle, Microsoft SQL Server etc.) is simply a matter of specifying the appropriate driver.

In addition to the above, we need to copy the jstl-1.2.jar dependency that we obtained above into the WEB-INF/lib folder so that the JSP templating engine can use it.

## JSP Source
Next we add a JSP file at `src/main/webapp/WEB-INF/pages` that holds the code for our dynamic HTML content where we show the list of authorised users in the database.

`src/main/webapp/WEB-INF/pages/authlist.jsp:`
```
<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<html>

<head>
    <title>Authorised users</title>
</head>

<body>

<h2>List of Authorised Users</h2>

<table>
    <c:forEach items="${list}" var="userlist">
        <tr>
            <td>
                <c:out value="${userlist}" />
            </td>
        </tr>
    </c:forEach>
</table>

</body>

</html>
```

Note that we define a variable called `userlist`. This is what we need to input to the View from the controller that we will invoke for this web app.

## Java source

Spring JDBC recommends that while accessing a database, we use a layer of abstraction around the database querying and editing. An object that does this is called a `Data Access Object`. Thanks to this abstraction, we can provide some specific data operations without exposing details of the database to the web application itself. In the `UserDAO` class below, we expose just one public function, that queries the database for the list of users in it and returns it as a list of `String` objects.

`src/main/java/com/vizexperts/springapptutorial/db/UserDAO.java:`
```java
package com.vizexperts.springtutorialapp.db;

import java.util.List;
import java.sql.SQLException;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

/**
* This class handles accessing authorised users list from PostgreSQL database.
*
*/
@Repository
public class UserDAO {

private JdbcTemplate jdbcTemplate;

@Autowired
public void setDataSource(DataSource dataSource) {
    this.jdbcTemplate = new JdbcTemplate(dataSource);
}

public List<String> getUsers() {

List<String> nameList = this.jdbcTemplate.query(
    "SELECT name FROM public.jdbcauth",
        new RowMapper<String>() {
            public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                String resultStr = new String();
                resultStr = rs.getString("name");
                return resultStr;
            }
        });

        return nameList;
     }
}
```
There are several new items of interest in the above code:

- @Repository annotation is a marker for any class that fulfills the role or stereotype (also known as Data Access Object or DAO) of a repository.
- @Autowired marks a constructor, field, setter method or config method as to be autowired by Spring's dependency injection facilities. What this means in this context is that setDataSource will be called by Spring, at application startup. It will take the data source we defined above in application.properties and set it as the JDBC source of this DAO.
- JdbcTemplate is a class provided by Spring that makes it easy to work with SQL relational databases and JDBC. JdbcTemplate class eliminates most errors that arise out of improper resource acquisition, connection management, exception handling, and general error checking. For eg. you may have opened the database but forgotten to close it. JdbcTemplate makes sure that the resource is properly initialized and cleaned up on end of query.
- RowMapper is an interface used by JdbcTemplate for mapping rows of a ResultSet on a per-row basis. Implementations of this interface perform the actual work of mapping each row to a result object, but don't need to worry about exception handling.

In addition to the above, we create a new controller that we will use to handle all incoming authorization related queries:
`src/main/java/com/vizexperts/springtutorialapp/AuthController.java`
```java
package com.vizexperts.springtutorialapp;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.beans.factory.annotation.Autowired;

import com.vizexperts.springtutorialapp.db.UserDAO;

@Controller
@RestController
public class AuthController {

    @Autowired
    UserDAO authDAO;

    @RequestMapping("/userlist")
    public ModelAndView fetchUserList() {
        List<String> userList = authDAO.getUsers();
        ModelAndView mav = new ModelAndView();
        mav.setViewName("authlist");
        mav.addObject("userlist", userList);
        return mav;
    }
}
```
There is only one new item of interest in the above code. Note that we use the @Autowired annotation in this code snippet as well. Note that nowhere in the code do we actually instantiate this object. The @Autowired annotation here makes sure that the Spring Application instantiates an object of this type for us at startup.

The rest of the code is very similar to the last sample that covers Spring MVC. We obtain the list of users using the `UserDAO` object and pass that information to the `ModelAndView` object (in this case a JSP file) to render out HTML with.

## Conclusion
We now know how to incorporate a relational database using JDBC interfaces that Spring provides. We can use the JDBCTemplate object to do other relational database queries/operations (such as inserting or deleting a new user).

</xmp>


<script src="http://strapdownjs.com/v/0.2/strapdown.js"></script>
</html>